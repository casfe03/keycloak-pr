package org.keycloak.subsystem.server.logging;

import java.util.Locale;
import java.io.Serializable;
import javax.annotation.Generated;

/**
 * Warning this class consists of generated code.
 */
@Generated(value = "org.jboss.logging.processor.generator.model.MessageBundleImplementor", date = "2021-12-02T22:59:10-0200")
public class KeycloakMessages_$bundle implements KeycloakMessages, Serializable {
    private static final long serialVersionUID = 1L;
    protected KeycloakMessages_$bundle() {}
    public static final KeycloakMessages_$bundle INSTANCE = new KeycloakMessages_$bundle();
    protected Object readResolve() {
        return INSTANCE;
    }
    private static final Locale LOCALE = Locale.ROOT;
    protected Locale getLoggingLocale() {
        return LOCALE;
    }
}
