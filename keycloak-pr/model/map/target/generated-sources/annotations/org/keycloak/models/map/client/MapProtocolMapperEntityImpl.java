package org.keycloak.models.map.client;
import java.util.Objects;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapProtocolMapperEntityImpl extends org.keycloak.models.map.common.UpdatableEntity.Impl implements org.keycloak.models.map.client.MapProtocolMapperEntity {
    public MapProtocolMapperEntityImpl() { super(); }
    @Override public boolean equals(Object o) {
        if (o == this) return true; 
        if (! (o instanceof MapProtocolMapperEntityImpl)) return false; 
        MapProtocolMapperEntityImpl other = (MapProtocolMapperEntityImpl) o; 
        return Objects.equals(getId(), other.getId())
          && Objects.equals(getConfig(), other.getConfig())
          && Objects.equals(getName(), other.getName())
          && Objects.equals(getProtocolMapper(), other.getProtocolMapper());
    }
    @Override public int hashCode() {
        return (getId() == null ? super.hashCode() : getId().hashCode());
    }
    @Override public String toString() {
        return String.format("%s@%08x", getId(), System.identityHashCode(this));
    }

    private String fId;
    @SuppressWarnings("unchecked") @Override public String getId() {
        return fId;
    }
    @SuppressWarnings("unchecked") @Override public void setId(String p0) {
        updated |= ! Objects.equals(fId, p0);
        fId = p0;
    }

    private java.util.Map<String,String> fConfig;
    @SuppressWarnings("unchecked") @Override public void setConfig(java.util.Map<String,String> p0) {
        p0 = p0 == null ? null : new java.util.HashMap<>(p0);
        updated |= ! Objects.equals(fConfig, p0);
        fConfig = p0;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,String> getConfig() {
        return fConfig;
    }

    private String fName;
    @SuppressWarnings("unchecked") @Override public String getName() {
        return fName;
    }
    @SuppressWarnings("unchecked") @Override public void setName(String p0) {
        updated |= ! Objects.equals(fName, p0);
        fName = p0;
    }

    private String fProtocolMapper;
    @SuppressWarnings("unchecked") @Override public String getProtocolMapper() {
        return fProtocolMapper;
    }
    @SuppressWarnings("unchecked") @Override public void setProtocolMapper(String p0) {
        updated |= ! Objects.equals(fProtocolMapper, p0);
        fProtocolMapper = p0;
    }
}
