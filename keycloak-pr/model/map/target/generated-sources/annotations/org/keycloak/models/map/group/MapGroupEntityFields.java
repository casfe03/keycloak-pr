package org.keycloak.models.map.group;
public enum MapGroupEntityFields {
    ID,
    ATTRIBUTES,
    GRANTED_ROLES,
    NAME,
    PARENT_ID,
    REALM_ID,
}
