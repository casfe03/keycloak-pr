package org.keycloak.models.map.group;
public class MapGroupEntityDelegate implements org.keycloak.models.map.group.MapGroupEntity {
    private final org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.group.MapGroupEntity> delegateProvider;
    public MapGroupEntityDelegate(org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.group.MapGroupEntity> delegateProvider) {
        this.delegateProvider = delegateProvider;
    }
    @Override public boolean isUpdated() {
        return delegateProvider.isUpdated();
    }
    @Override public String getId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.ID).getId();
    }
    @Override public void setId(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.ID).setId(id);
    }
    @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.ATTRIBUTES).getAttributes();
    }
    @Override public void setAttributes(java.util.Map<String,java.util.List<String>> attributes) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.ATTRIBUTES).setAttributes(attributes);
    }
    @Override public java.util.List<String> getAttribute(String name) {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.ATTRIBUTES).getAttribute(name);
    }
    @Override public void setAttribute(String name, java.util.List<String> value) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.ATTRIBUTES).setAttribute(name, value);
    }
    @Override public void removeAttribute(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.ATTRIBUTES).removeAttribute(name);
    }
    @Override public String getName() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.NAME).getName();
    }
    @Override public void setName(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.NAME).setName(name);
    }
    @Override public String getParentId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.PARENT_ID).getParentId();
    }
    @Override public void setParentId(String parentId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.PARENT_ID).setParentId(parentId);
    }
    @Override public String getRealmId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.REALM_ID).getRealmId();
    }
    @Override public void setRealmId(String realmId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.REALM_ID).setRealmId(realmId);
    }
    @Override public java.util.Set<String> getGrantedRoles() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.group.MapGroupEntityFields.GRANTED_ROLES).getGrantedRoles();
    }
    @Override public void setGrantedRoles(java.util.Set<String> grantedRoles) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.GRANTED_ROLES).setGrantedRoles(grantedRoles);
    }
    @Override public void addGrantedRole(String role) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.GRANTED_ROLES).addGrantedRole(role);
    }
    @Override public void removeGrantedRole(String role) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.group.MapGroupEntityFields.GRANTED_ROLES).removeGrantedRole(role);
    }
}
