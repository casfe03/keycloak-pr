package org.keycloak.models.map.client;
public class MapProtocolMapperEntityDelegate implements org.keycloak.models.map.client.MapProtocolMapperEntity {
    private final org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.client.MapProtocolMapperEntity> delegateProvider;
    public MapProtocolMapperEntityDelegate(org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.client.MapProtocolMapperEntity> delegateProvider) {
        this.delegateProvider = delegateProvider;
    }
    @Override public boolean isUpdated() {
        return delegateProvider.isUpdated();
    }
    @Override public String getId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapProtocolMapperEntityFields.ID).getId();
    }
    @Override public void setId(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapProtocolMapperEntityFields.ID).setId(id);
    }
    @Override public String getName() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapProtocolMapperEntityFields.NAME).getName();
    }
    @Override public void setName(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapProtocolMapperEntityFields.NAME).setName(name);
    }
    @Override public String getProtocolMapper() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapProtocolMapperEntityFields.PROTOCOL_MAPPER).getProtocolMapper();
    }
    @Override public void setProtocolMapper(String protocolMapper) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapProtocolMapperEntityFields.PROTOCOL_MAPPER).setProtocolMapper(protocolMapper);
    }
    @Override public java.util.Map<String,String> getConfig() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapProtocolMapperEntityFields.CONFIG).getConfig();
    }
    @Override public void setConfig(java.util.Map<String,String> config) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapProtocolMapperEntityFields.CONFIG).setConfig(config);
    }
}
