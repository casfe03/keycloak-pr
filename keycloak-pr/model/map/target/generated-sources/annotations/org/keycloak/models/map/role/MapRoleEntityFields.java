package org.keycloak.models.map.role;
public enum MapRoleEntityFields {
    ID,
    ATTRIBUTES,
    CLIENT_ID,
    CLIENT_ROLE,
    COMPOSITE_ROLES,
    DESCRIPTION,
    NAME,
    REALM_ID,
}
