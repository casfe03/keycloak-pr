package org.keycloak.models.map.group;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapGroupEntityCloner {
    public static org.keycloak.models.map.group.MapGroupEntity deepClone(org.keycloak.models.map.group.MapGroupEntity original, org.keycloak.models.map.group.MapGroupEntity target) {
        target.setAttributes(original.getAttributes());
        target.setGrantedRoles(original.getGrantedRoles());
        target.setId(original.getId());
        target.setName(original.getName());
        target.setParentId(original.getParentId());
        target.setRealmId(original.getRealmId());
        target.clearUpdatedFlag();
        return target;
    }
    public static org.keycloak.models.map.group.MapGroupEntity deepCloneNoId(org.keycloak.models.map.group.MapGroupEntity original, org.keycloak.models.map.group.MapGroupEntity target) {
        target.setAttributes(original.getAttributes());
        target.setGrantedRoles(original.getGrantedRoles());
        target.setName(original.getName());
        target.setParentId(original.getParentId());
        target.setRealmId(original.getRealmId());
        target.clearUpdatedFlag();
        return target;
    }
}
