package org.keycloak.models.map.client;
public class MapClientEntityDelegate implements org.keycloak.models.map.client.MapClientEntity {
    private final org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.client.MapClientEntity> delegateProvider;
    public MapClientEntityDelegate(org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.client.MapClientEntity> delegateProvider) {
        this.delegateProvider = delegateProvider;
    }
    @Override public boolean isUpdated() {
        return delegateProvider.isUpdated();
    }
    @Override public String getId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.ID).getId();
    }
    @Override public void setId(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ID).setId(id);
    }
    @Override public java.util.Map<String,Boolean> getClientScopes() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_SCOPES).getClientScopes();
    }
    @Override public java.util.stream.Stream<String> getClientScopes(boolean defaultScope) {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_SCOPES).getClientScopes(defaultScope);
    }
    @Override public void setClientScope(String id, Boolean defaultScope) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_SCOPES).setClientScope(id, defaultScope);
    }
    @Override public void removeClientScope(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_SCOPES).removeClientScope(id);
    }
    @Override public org.keycloak.models.map.client.MapProtocolMapperEntity getProtocolMapper(String id) {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL_MAPPERS).getProtocolMapper(id);
    }
    @Override public java.util.Map<String,org.keycloak.models.map.client.MapProtocolMapperEntity> getProtocolMappers() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL_MAPPERS).getProtocolMappers();
    }
    @Override public void setProtocolMapper(String id, org.keycloak.models.map.client.MapProtocolMapperEntity mapping) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL_MAPPERS).setProtocolMapper(id, mapping);
    }
    @Override public void removeProtocolMapper(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL_MAPPERS).removeProtocolMapper(id);
    }
    @Override public void addRedirectUri(String redirectUri) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.REDIRECT_URIS).addRedirectUri(redirectUri);
    }
    @Override public java.util.Set<String> getRedirectUris() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.REDIRECT_URIS).getRedirectUris();
    }
    @Override public void removeRedirectUri(String redirectUri) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.REDIRECT_URIS).removeRedirectUri(redirectUri);
    }
    @Override public void setRedirectUris(java.util.Set<String> redirectUris) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.REDIRECT_URIS).setRedirectUris(redirectUris);
    }
    @Override public void addScopeMapping(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SCOPE_MAPPINGS).addScopeMapping(id);
    }
    @Override public void removeScopeMapping(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SCOPE_MAPPINGS).removeScopeMapping(id);
    }
    @Override public java.util.Collection<String> getScopeMappings() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.SCOPE_MAPPINGS).getScopeMappings();
    }
    @Override public void addWebOrigin(String webOrigin) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.WEB_ORIGINS).addWebOrigin(webOrigin);
    }
    @Override public java.util.Set<String> getWebOrigins() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.WEB_ORIGINS).getWebOrigins();
    }
    @Override public void removeWebOrigin(String webOrigin) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.WEB_ORIGINS).removeWebOrigin(webOrigin);
    }
    @Override public void setWebOrigins(java.util.Set<String> webOrigins) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.WEB_ORIGINS).setWebOrigins(webOrigins);
    }
    @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.ATTRIBUTES).getAttributes();
    }
    @Override public void removeAttribute(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ATTRIBUTES).removeAttribute(name);
    }
    @Override public void setAttribute(String name, java.util.List<String> values) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ATTRIBUTES).setAttribute(name, values);
    }
    @Override public java.util.Map<String,String> getAuthFlowBindings() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.AUTH_FLOW_BINDINGS).getAuthFlowBindings();
    }
    @Override public void setAuthFlowBindings(java.util.Map<String,String> authFlowBindings) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.AUTH_FLOW_BINDINGS).setAuthFlowBindings(authFlowBindings);
    }
    @Override public String getAuthenticationFlowBindingOverride(String binding) {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.AUTHENTICATION_FLOW_BINDING_OVERRIDES).getAuthenticationFlowBindingOverride(binding);
    }
    @Override public java.util.Map<String,String> getAuthenticationFlowBindingOverrides() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.AUTHENTICATION_FLOW_BINDING_OVERRIDES).getAuthenticationFlowBindingOverrides();
    }
    @Override public void removeAuthenticationFlowBindingOverride(String binding) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.AUTHENTICATION_FLOW_BINDING_OVERRIDES).removeAuthenticationFlowBindingOverride(binding);
    }
    @Override public void setAuthenticationFlowBindingOverride(String binding, String flowId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.AUTHENTICATION_FLOW_BINDING_OVERRIDES).setAuthenticationFlowBindingOverride(binding, flowId);
    }
    @Override public String getBaseUrl() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.BASE_URL).getBaseUrl();
    }
    @Override public String getClientAuthenticatorType() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_AUTHENTICATOR_TYPE).getClientAuthenticatorType();
    }
    @Override public String getClientId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_ID).getClientId();
    }
    @Override public String getDescription() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.DESCRIPTION).getDescription();
    }
    @Override public String getManagementUrl() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.MANAGEMENT_URL).getManagementUrl();
    }
    @Override public String getName() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.NAME).getName();
    }
    @Override public Integer getNodeReRegistrationTimeout() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.NODE_RE_REGISTRATION_TIMEOUT).getNodeReRegistrationTimeout();
    }
    @Override public Integer getNotBefore() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.NOT_BEFORE).getNotBefore();
    }
    @Override public String getProtocol() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL).getProtocol();
    }
    @Override public String getRealmId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.REALM_ID).getRealmId();
    }
    @Override public String getRegistrationToken() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.REGISTRATION_TOKEN).getRegistrationToken();
    }
    @Override public String getRootUrl() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.ROOT_URL).getRootUrl();
    }
    @Override public java.util.Set<String> getScope() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.SCOPE).getScope();
    }
    @Override public String getSecret() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.SECRET).getSecret();
    }
    @Override public Boolean isAlwaysDisplayInConsole() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.ALWAYS_DISPLAY_IN_CONSOLE).isAlwaysDisplayInConsole();
    }
    @Override public Boolean isBearerOnly() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.BEARER_ONLY).isBearerOnly();
    }
    @Override public Boolean isConsentRequired() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.CONSENT_REQUIRED).isConsentRequired();
    }
    @Override public Boolean isDirectAccessGrantsEnabled() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.DIRECT_ACCESS_GRANTS_ENABLED).isDirectAccessGrantsEnabled();
    }
    @Override public Boolean isEnabled() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.ENABLED).isEnabled();
    }
    @Override public Boolean isFrontchannelLogout() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.FRONTCHANNEL_LOGOUT).isFrontchannelLogout();
    }
    @Override public Boolean isFullScopeAllowed() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.FULL_SCOPE_ALLOWED).isFullScopeAllowed();
    }
    @Override public Boolean isImplicitFlowEnabled() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.IMPLICIT_FLOW_ENABLED).isImplicitFlowEnabled();
    }
    @Override public Boolean isPublicClient() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.PUBLIC_CLIENT).isPublicClient();
    }
    @Override public Boolean isServiceAccountsEnabled() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.SERVICE_ACCOUNTS_ENABLED).isServiceAccountsEnabled();
    }
    @Override public Boolean isStandardFlowEnabled() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.STANDARD_FLOW_ENABLED).isStandardFlowEnabled();
    }
    @Override public Boolean isSurrogateAuthRequired() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.client.MapClientEntityFields.SURROGATE_AUTH_REQUIRED).isSurrogateAuthRequired();
    }
    @Override public void setAlwaysDisplayInConsole(Boolean alwaysDisplayInConsole) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ALWAYS_DISPLAY_IN_CONSOLE).setAlwaysDisplayInConsole(alwaysDisplayInConsole);
    }
    @Override public void setBaseUrl(String baseUrl) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.BASE_URL).setBaseUrl(baseUrl);
    }
    @Override public void setBearerOnly(Boolean bearerOnly) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.BEARER_ONLY).setBearerOnly(bearerOnly);
    }
    @Override public void setClientAuthenticatorType(String clientAuthenticatorType) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_AUTHENTICATOR_TYPE).setClientAuthenticatorType(clientAuthenticatorType);
    }
    @Override public void setClientId(String clientId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.CLIENT_ID).setClientId(clientId);
    }
    @Override public void setConsentRequired(Boolean consentRequired) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.CONSENT_REQUIRED).setConsentRequired(consentRequired);
    }
    @Override public void setDescription(String description) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.DESCRIPTION).setDescription(description);
    }
    @Override public void setDirectAccessGrantsEnabled(Boolean directAccessGrantsEnabled) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.DIRECT_ACCESS_GRANTS_ENABLED).setDirectAccessGrantsEnabled(directAccessGrantsEnabled);
    }
    @Override public void setEnabled(Boolean enabled) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ENABLED).setEnabled(enabled);
    }
    @Override public void setFrontchannelLogout(Boolean frontchannelLogout) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.FRONTCHANNEL_LOGOUT).setFrontchannelLogout(frontchannelLogout);
    }
    @Override public void setFullScopeAllowed(Boolean fullScopeAllowed) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.FULL_SCOPE_ALLOWED).setFullScopeAllowed(fullScopeAllowed);
    }
    @Override public void setImplicitFlowEnabled(Boolean implicitFlowEnabled) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.IMPLICIT_FLOW_ENABLED).setImplicitFlowEnabled(implicitFlowEnabled);
    }
    @Override public void setManagementUrl(String managementUrl) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.MANAGEMENT_URL).setManagementUrl(managementUrl);
    }
    @Override public void setName(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.NAME).setName(name);
    }
    @Override public void setNodeReRegistrationTimeout(Integer nodeReRegistrationTimeout) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.NODE_RE_REGISTRATION_TIMEOUT).setNodeReRegistrationTimeout(nodeReRegistrationTimeout);
    }
    @Override public void setNotBefore(Integer notBefore) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.NOT_BEFORE).setNotBefore(notBefore);
    }
    @Override public void setProtocol(String protocol) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.PROTOCOL).setProtocol(protocol);
    }
    @Override public void setPublicClient(Boolean publicClient) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.PUBLIC_CLIENT).setPublicClient(publicClient);
    }
    @Override public void setRealmId(String realmId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.REALM_ID).setRealmId(realmId);
    }
    @Override public void setRegistrationToken(String registrationToken) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.REGISTRATION_TOKEN).setRegistrationToken(registrationToken);
    }
    @Override public void setRootUrl(String rootUrl) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.ROOT_URL).setRootUrl(rootUrl);
    }
    @Override public void setScope(java.util.Set<String> scope) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SCOPE).setScope(scope);
    }
    @Override public void setSecret(String secret) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SECRET).setSecret(secret);
    }
    @Override public void setServiceAccountsEnabled(Boolean serviceAccountsEnabled) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SERVICE_ACCOUNTS_ENABLED).setServiceAccountsEnabled(serviceAccountsEnabled);
    }
    @Override public void setStandardFlowEnabled(Boolean standardFlowEnabled) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.STANDARD_FLOW_ENABLED).setStandardFlowEnabled(standardFlowEnabled);
    }
    @Override public void setSurrogateAuthRequired(Boolean surrogateAuthRequired) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.client.MapClientEntityFields.SURROGATE_AUTH_REQUIRED).setSurrogateAuthRequired(surrogateAuthRequired);
    }
}
