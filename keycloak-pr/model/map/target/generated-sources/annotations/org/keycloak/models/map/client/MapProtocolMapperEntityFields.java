package org.keycloak.models.map.client;
public enum MapProtocolMapperEntityFields {
    ID,
    CONFIG,
    NAME,
    PROTOCOL_MAPPER,
}
