package org.keycloak.models.map.client;
import java.util.Objects;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapClientEntityImpl extends org.keycloak.models.map.client.MapClientEntity.AbstractClientEntity implements org.keycloak.models.map.client.MapClientEntity {
    /**
     * @deprecated This constructor uses a {@link DeepCloner#DUMB_CLONER} that does not clone anything. Use {@link #MapClientEntityImpl(DeepCloner)} variant instead
     */
    public MapClientEntityImpl() { this(DeepCloner.DUMB_CLONER); }
    public MapClientEntityImpl(DeepCloner cloner) { super(); this.cloner = cloner; }
    @Override public boolean equals(Object o) {
        if (o == this) return true; 
        if (! (o instanceof MapClientEntityImpl)) return false; 
        MapClientEntityImpl other = (MapClientEntityImpl) o; 
        return Objects.equals(getId(), other.getId())
          && Objects.equals(getAttributes(), other.getAttributes())
          && Objects.equals(getAuthFlowBindings(), other.getAuthFlowBindings())
          && Objects.equals(getAuthenticationFlowBindingOverrides(), other.getAuthenticationFlowBindingOverrides())
          && Objects.equals(getBaseUrl(), other.getBaseUrl())
          && Objects.equals(getClientAuthenticatorType(), other.getClientAuthenticatorType())
          && Objects.equals(getClientId(), other.getClientId())
          && Objects.equals(getClientScopes(), other.getClientScopes())
          && Objects.equals(getDescription(), other.getDescription())
          && Objects.equals(getManagementUrl(), other.getManagementUrl())
          && Objects.equals(getName(), other.getName())
          && Objects.equals(getNodeReRegistrationTimeout(), other.getNodeReRegistrationTimeout())
          && Objects.equals(getNotBefore(), other.getNotBefore())
          && Objects.equals(getProtocol(), other.getProtocol())
          && Objects.equals(getProtocolMappers(), other.getProtocolMappers())
          && Objects.equals(getRealmId(), other.getRealmId())
          && Objects.equals(getRedirectUris(), other.getRedirectUris())
          && Objects.equals(getRegistrationToken(), other.getRegistrationToken())
          && Objects.equals(getRootUrl(), other.getRootUrl())
          && Objects.equals(getScope(), other.getScope())
          && Objects.equals(getScopeMappings(), other.getScopeMappings())
          && Objects.equals(getSecret(), other.getSecret())
          && Objects.equals(getWebOrigins(), other.getWebOrigins())
          && Objects.equals(isAlwaysDisplayInConsole(), other.isAlwaysDisplayInConsole())
          && Objects.equals(isBearerOnly(), other.isBearerOnly())
          && Objects.equals(isConsentRequired(), other.isConsentRequired())
          && Objects.equals(isDirectAccessGrantsEnabled(), other.isDirectAccessGrantsEnabled())
          && Objects.equals(isEnabled(), other.isEnabled())
          && Objects.equals(isFrontchannelLogout(), other.isFrontchannelLogout())
          && Objects.equals(isFullScopeAllowed(), other.isFullScopeAllowed())
          && Objects.equals(isImplicitFlowEnabled(), other.isImplicitFlowEnabled())
          && Objects.equals(isPublicClient(), other.isPublicClient())
          && Objects.equals(isServiceAccountsEnabled(), other.isServiceAccountsEnabled())
          && Objects.equals(isStandardFlowEnabled(), other.isStandardFlowEnabled())
          && Objects.equals(isSurrogateAuthRequired(), other.isSurrogateAuthRequired());
    }
    @Override public int hashCode() {
        return (getId() == null ? super.hashCode() : getId().hashCode());
    }
    @Override public String toString() {
        return String.format("%s@%08x", getId(), System.identityHashCode(this));
    }
    private final DeepCloner cloner;
    public <V> V deepClone(V obj) {
        return cloner.from(obj);
    }

    private String fId;

    private Boolean fAlwaysDisplayInConsole;
    @SuppressWarnings("unchecked") @Override public void setAlwaysDisplayInConsole(Boolean p0) {
        updated |= ! Objects.equals(fAlwaysDisplayInConsole, p0);
        fAlwaysDisplayInConsole = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isAlwaysDisplayInConsole() {
        return fAlwaysDisplayInConsole;
    }

    private java.util.Map<String,java.util.List<String>> fAttributes;
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return fAttributes;
    }
    @SuppressWarnings("unchecked") @Override public void setAttribute(String p0, java.util.List<String> p1) {
        if (fAttributes == null) { fAttributes = new java.util.HashMap<>(); }
        p1 = p1 == null ? null : new java.util.LinkedList<>(p1);
        Object v = fAttributes.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public void removeAttribute(String p0) {
        if (fAttributes == null) { return; }
        boolean removed = fAttributes.remove(p0) != null;
        updated |= removed;
    }

    private java.util.Map<String,String> fAuthFlowBindings;
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,String> getAuthFlowBindings() {
        return fAuthFlowBindings;
    }
    @SuppressWarnings("unchecked") @Override public void setAuthFlowBindings(java.util.Map<String,String> p0) {
        p0 = p0 == null ? null : new java.util.HashMap<>(p0);
        updated |= ! Objects.equals(fAuthFlowBindings, p0);
        fAuthFlowBindings = p0;
    }

    private java.util.Map<String,String> fAuthenticationFlowBindingOverrides;
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,String> getAuthenticationFlowBindingOverrides() {
        return fAuthenticationFlowBindingOverrides;
    }
    @SuppressWarnings("unchecked") @Override public String getAuthenticationFlowBindingOverride(String p0) {
        return fAuthenticationFlowBindingOverrides == null ? null : fAuthenticationFlowBindingOverrides.get(p0);
    }
    @SuppressWarnings("unchecked") @Override public void setAuthenticationFlowBindingOverride(String p0, String p1) {
        if (fAuthenticationFlowBindingOverrides == null) { fAuthenticationFlowBindingOverrides = new java.util.HashMap<>(); }
        Object v = fAuthenticationFlowBindingOverrides.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public void removeAuthenticationFlowBindingOverride(String p0) {
        if (fAuthenticationFlowBindingOverrides == null) { return; }
        boolean removed = fAuthenticationFlowBindingOverrides.remove(p0) != null;
        updated |= removed;
    }

    private String fBaseUrl;
    @SuppressWarnings("unchecked") @Override public void setBaseUrl(String p0) {
        updated |= ! Objects.equals(fBaseUrl, p0);
        fBaseUrl = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getBaseUrl() {
        return fBaseUrl;
    }

    private Boolean fBearerOnly;
    @SuppressWarnings("unchecked") @Override public void setBearerOnly(Boolean p0) {
        updated |= ! Objects.equals(fBearerOnly, p0);
        fBearerOnly = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isBearerOnly() {
        return fBearerOnly;
    }

    private String fClientAuthenticatorType;
    @SuppressWarnings("unchecked") @Override public void setClientAuthenticatorType(String p0) {
        updated |= ! Objects.equals(fClientAuthenticatorType, p0);
        fClientAuthenticatorType = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getClientAuthenticatorType() {
        return fClientAuthenticatorType;
    }

    private String fClientId;
    @SuppressWarnings("unchecked") @Override public void setClientId(String p0) {
        updated |= ! Objects.equals(fClientId, p0);
        fClientId = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getClientId() {
        return fClientId;
    }

    private java.util.Map<String,Boolean> fClientScopes;
    @SuppressWarnings("unchecked") @Override public void removeClientScope(String p0) {
        if (fClientScopes == null) { return; }
        boolean removed = fClientScopes.remove(p0) != null;
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public void setClientScope(String p0, Boolean p1) {
        if (fClientScopes == null) { fClientScopes = new java.util.HashMap<>(); }
        Object v = fClientScopes.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,Boolean> getClientScopes() {
        return fClientScopes;
    }

    private Boolean fConsentRequired;
    @SuppressWarnings("unchecked") @Override public void setConsentRequired(Boolean p0) {
        updated |= ! Objects.equals(fConsentRequired, p0);
        fConsentRequired = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isConsentRequired() {
        return fConsentRequired;
    }

    private String fDescription;
    @SuppressWarnings("unchecked") @Override public void setDescription(String p0) {
        updated |= ! Objects.equals(fDescription, p0);
        fDescription = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getDescription() {
        return fDescription;
    }

    private Boolean fDirectAccessGrantsEnabled;
    @SuppressWarnings("unchecked") @Override public void setDirectAccessGrantsEnabled(Boolean p0) {
        updated |= ! Objects.equals(fDirectAccessGrantsEnabled, p0);
        fDirectAccessGrantsEnabled = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isDirectAccessGrantsEnabled() {
        return fDirectAccessGrantsEnabled;
    }

    private Boolean fEnabled;
    @SuppressWarnings("unchecked") @Override public Boolean isEnabled() {
        return fEnabled;
    }
    @SuppressWarnings("unchecked") @Override public void setEnabled(Boolean p0) {
        updated |= ! Objects.equals(fEnabled, p0);
        fEnabled = p0;
    }

    private Boolean fFrontchannelLogout;
    @SuppressWarnings("unchecked") @Override public void setFrontchannelLogout(Boolean p0) {
        updated |= ! Objects.equals(fFrontchannelLogout, p0);
        fFrontchannelLogout = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isFrontchannelLogout() {
        return fFrontchannelLogout;
    }

    private Boolean fFullScopeAllowed;
    @SuppressWarnings("unchecked") @Override public void setFullScopeAllowed(Boolean p0) {
        updated |= ! Objects.equals(fFullScopeAllowed, p0);
        fFullScopeAllowed = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isFullScopeAllowed() {
        return fFullScopeAllowed;
    }

    private Boolean fImplicitFlowEnabled;
    @SuppressWarnings("unchecked") @Override public void setImplicitFlowEnabled(Boolean p0) {
        updated |= ! Objects.equals(fImplicitFlowEnabled, p0);
        fImplicitFlowEnabled = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isImplicitFlowEnabled() {
        return fImplicitFlowEnabled;
    }

    private String fManagementUrl;
    @SuppressWarnings("unchecked") @Override public String getManagementUrl() {
        return fManagementUrl;
    }
    @SuppressWarnings("unchecked") @Override public void setManagementUrl(String p0) {
        updated |= ! Objects.equals(fManagementUrl, p0);
        fManagementUrl = p0;
    }

    private String fName;
    @SuppressWarnings("unchecked") @Override public String getName() {
        return fName;
    }
    @SuppressWarnings("unchecked") @Override public void setName(String p0) {
        updated |= ! Objects.equals(fName, p0);
        fName = p0;
    }

    private Integer fNodeReRegistrationTimeout;
    @SuppressWarnings("unchecked") @Override public Integer getNodeReRegistrationTimeout() {
        return fNodeReRegistrationTimeout;
    }
    @SuppressWarnings("unchecked") @Override public void setNodeReRegistrationTimeout(Integer p0) {
        updated |= ! Objects.equals(fNodeReRegistrationTimeout, p0);
        fNodeReRegistrationTimeout = p0;
    }

    private Integer fNotBefore;
    @SuppressWarnings("unchecked") @Override public void setNotBefore(Integer p0) {
        updated |= ! Objects.equals(fNotBefore, p0);
        fNotBefore = p0;
    }
    @SuppressWarnings("unchecked") @Override public Integer getNotBefore() {
        return fNotBefore;
    }

    private String fProtocol;
    @SuppressWarnings("unchecked") @Override public void setProtocol(String p0) {
        updated |= ! Objects.equals(fProtocol, p0);
        fProtocol = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getProtocol() {
        return fProtocol;
    }

    private java.util.Map<String,org.keycloak.models.map.client.MapProtocolMapperEntity> fProtocolMappers;
    @SuppressWarnings("unchecked") @Override public void removeProtocolMapper(String p0) {
        if (fProtocolMappers == null) { return; }
        boolean removed = fProtocolMappers.remove(p0) != null;
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public void setProtocolMapper(String p0, org.keycloak.models.map.client.MapProtocolMapperEntity p1) {
        if (fProtocolMappers == null) { fProtocolMappers = new java.util.HashMap<>(); }
        p1 = deepClone(p1);
        Object v = fProtocolMappers.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public org.keycloak.models.map.client.MapProtocolMapperEntity getProtocolMapper(String p0) {
        return fProtocolMappers == null ? null : fProtocolMappers.get(p0);
    }
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,org.keycloak.models.map.client.MapProtocolMapperEntity> getProtocolMappers() {
        return fProtocolMappers;
    }

    private Boolean fPublicClient;
    @SuppressWarnings("unchecked") @Override public void setPublicClient(Boolean p0) {
        updated |= ! Objects.equals(fPublicClient, p0);
        fPublicClient = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isPublicClient() {
        return fPublicClient;
    }

    private String fRealmId;
    @SuppressWarnings("unchecked") @Override public void setRealmId(String p0) {
        updated |= ! Objects.equals(fRealmId, p0);
        fRealmId = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getRealmId() {
        return fRealmId;
    }

    private java.util.Set<String> fRedirectUris;
    @SuppressWarnings("unchecked") @Override public void removeRedirectUri(String p0) {
        if (fRedirectUris == null) { return; }
        boolean removed = fRedirectUris.remove(p0);
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public void addRedirectUri(String p0) {
        if (fRedirectUris == null) { fRedirectUris = new java.util.HashSet<>(); }
        updated |= fRedirectUris.add(p0);
    }
    @SuppressWarnings("unchecked") @Override public void setRedirectUris(java.util.Set<String> p0) {
        p0 = p0 == null ? null : new java.util.HashSet<>(p0);
        updated |= ! Objects.equals(fRedirectUris, p0);
        fRedirectUris = p0;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Set<String> getRedirectUris() {
        return fRedirectUris;
    }

    private String fRegistrationToken;
    @SuppressWarnings("unchecked") @Override public void setRegistrationToken(String p0) {
        updated |= ! Objects.equals(fRegistrationToken, p0);
        fRegistrationToken = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getRegistrationToken() {
        return fRegistrationToken;
    }

    private String fRootUrl;
    @SuppressWarnings("unchecked") @Override public String getRootUrl() {
        return fRootUrl;
    }
    @SuppressWarnings("unchecked") @Override public void setRootUrl(String p0) {
        updated |= ! Objects.equals(fRootUrl, p0);
        fRootUrl = p0;
    }

    private java.util.Set<String> fScope;
    @SuppressWarnings("unchecked") @Override public java.util.Set<String> getScope() {
        return fScope;
    }
    @SuppressWarnings("unchecked") @Override public void setScope(java.util.Set<String> p0) {
        p0 = p0 == null ? null : new java.util.HashSet<>(p0);
        updated |= ! Objects.equals(fScope, p0);
        fScope = p0;
    }

    private java.util.Collection<String> fScopeMappings;
    @SuppressWarnings("unchecked") @Override public void addScopeMapping(String p0) {
        if (fScopeMappings == null) { fScopeMappings = new java.util.LinkedList<>(); }
        fScopeMappings.add(p0);
        updated = true;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Collection<String> getScopeMappings() {
        return fScopeMappings;
    }
    @SuppressWarnings("unchecked") @Override public void removeScopeMapping(String p0) {
        if (fScopeMappings == null) { return; }
        boolean removed = fScopeMappings.remove(p0);
        updated |= removed;
    }

    private String fSecret;
    @SuppressWarnings("unchecked") @Override public void setSecret(String p0) {
        updated |= ! Objects.equals(fSecret, p0);
        fSecret = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getSecret() {
        return fSecret;
    }

    private Boolean fServiceAccountsEnabled;
    @SuppressWarnings("unchecked") @Override public Boolean isServiceAccountsEnabled() {
        return fServiceAccountsEnabled;
    }
    @SuppressWarnings("unchecked") @Override public void setServiceAccountsEnabled(Boolean p0) {
        updated |= ! Objects.equals(fServiceAccountsEnabled, p0);
        fServiceAccountsEnabled = p0;
    }

    private Boolean fStandardFlowEnabled;
    @SuppressWarnings("unchecked") @Override public Boolean isStandardFlowEnabled() {
        return fStandardFlowEnabled;
    }
    @SuppressWarnings("unchecked") @Override public void setStandardFlowEnabled(Boolean p0) {
        updated |= ! Objects.equals(fStandardFlowEnabled, p0);
        fStandardFlowEnabled = p0;
    }

    private Boolean fSurrogateAuthRequired;
    @SuppressWarnings("unchecked") @Override public Boolean isSurrogateAuthRequired() {
        return fSurrogateAuthRequired;
    }
    @SuppressWarnings("unchecked") @Override public void setSurrogateAuthRequired(Boolean p0) {
        updated |= ! Objects.equals(fSurrogateAuthRequired, p0);
        fSurrogateAuthRequired = p0;
    }

    private java.util.Set<String> fWebOrigins;
    @SuppressWarnings("unchecked") @Override public java.util.Set<String> getWebOrigins() {
        return fWebOrigins;
    }
    @SuppressWarnings("unchecked") @Override public void addWebOrigin(String p0) {
        if (fWebOrigins == null) { fWebOrigins = new java.util.HashSet<>(); }
        updated |= fWebOrigins.add(p0);
    }
    @SuppressWarnings("unchecked") @Override public void setWebOrigins(java.util.Set<String> p0) {
        p0 = p0 == null ? null : new java.util.HashSet<>(p0);
        updated |= ! Objects.equals(fWebOrigins, p0);
        fWebOrigins = p0;
    }
    @SuppressWarnings("unchecked") @Override public void removeWebOrigin(String p0) {
        if (fWebOrigins == null) { return; }
        boolean removed = fWebOrigins.remove(p0);
        updated |= removed;
    }
}
