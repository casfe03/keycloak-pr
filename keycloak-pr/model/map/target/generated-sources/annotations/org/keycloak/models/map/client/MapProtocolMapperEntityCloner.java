package org.keycloak.models.map.client;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapProtocolMapperEntityCloner {
    public static org.keycloak.models.map.client.MapProtocolMapperEntity deepClone(org.keycloak.models.map.client.MapProtocolMapperEntity original, org.keycloak.models.map.client.MapProtocolMapperEntity target) {
        target.setConfig(original.getConfig());
        target.setId(original.getId());
        target.setName(original.getName());
        target.setProtocolMapper(original.getProtocolMapper());
        target.clearUpdatedFlag();
        return target;
    }
    public static org.keycloak.models.map.client.MapProtocolMapperEntity deepCloneNoId(org.keycloak.models.map.client.MapProtocolMapperEntity original, org.keycloak.models.map.client.MapProtocolMapperEntity target) {
        target.setConfig(original.getConfig());
        target.setName(original.getName());
        target.setProtocolMapper(original.getProtocolMapper());
        target.clearUpdatedFlag();
        return target;
    }
}
