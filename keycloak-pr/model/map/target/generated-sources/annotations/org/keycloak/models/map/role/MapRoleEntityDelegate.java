package org.keycloak.models.map.role;
public class MapRoleEntityDelegate implements org.keycloak.models.map.role.MapRoleEntity {
    private final org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.role.MapRoleEntity> delegateProvider;
    public MapRoleEntityDelegate(org.keycloak.models.map.common.delegate.DelegateProvider<org.keycloak.models.map.role.MapRoleEntity> delegateProvider) {
        this.delegateProvider = delegateProvider;
    }
    @Override public boolean isUpdated() {
        return delegateProvider.isUpdated();
    }
    @Override public String getId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.ID).getId();
    }
    @Override public void setId(String id) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.ID).setId(id);
    }
    @Override public Boolean isClientRole() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.CLIENT_ROLE).isClientRole();
    }
    @Override public String getRealmId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.REALM_ID).getRealmId();
    }
    @Override public String getClientId() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.CLIENT_ID).getClientId();
    }
    @Override public String getName() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.NAME).getName();
    }
    @Override public String getDescription() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.DESCRIPTION).getDescription();
    }
    @Override public void setClientRole(Boolean clientRole) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.CLIENT_ROLE).setClientRole(clientRole);
    }
    @Override public void setRealmId(String realmId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.REALM_ID).setRealmId(realmId);
    }
    @Override public void setClientId(String clientId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.CLIENT_ID).setClientId(clientId);
    }
    @Override public void setName(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.NAME).setName(name);
    }
    @Override public void setDescription(String description) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.DESCRIPTION).setDescription(description);
    }
    @Override public java.util.Set<String> getCompositeRoles() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.COMPOSITE_ROLES).getCompositeRoles();
    }
    @Override public void setCompositeRoles(java.util.Set<String> compositeRoles) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.COMPOSITE_ROLES).setCompositeRoles(compositeRoles);
    }
    @Override public void addCompositeRole(String roleId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.COMPOSITE_ROLES).addCompositeRole(roleId);
    }
    @Override public void removeCompositeRole(String roleId) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.COMPOSITE_ROLES).removeCompositeRole(roleId);
    }
    @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return delegateProvider.getDelegate(true, org.keycloak.models.map.role.MapRoleEntityFields.ATTRIBUTES).getAttributes();
    }
    @Override public void setAttributes(java.util.Map<String,java.util.List<String>> attributes) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.ATTRIBUTES).setAttributes(attributes);
    }
    @Override public void setAttribute(String name, java.util.List<String> values) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.ATTRIBUTES).setAttribute(name, values);
    }
    @Override public void removeAttribute(String name) {
        delegateProvider.getDelegate(false, org.keycloak.models.map.role.MapRoleEntityFields.ATTRIBUTES).removeAttribute(name);
    }
}
