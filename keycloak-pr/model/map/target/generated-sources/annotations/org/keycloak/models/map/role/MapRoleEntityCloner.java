package org.keycloak.models.map.role;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapRoleEntityCloner {
    public static org.keycloak.models.map.role.MapRoleEntity deepClone(org.keycloak.models.map.role.MapRoleEntity original, org.keycloak.models.map.role.MapRoleEntity target) {
        target.setAttributes(original.getAttributes());
        target.setClientId(original.getClientId());
        target.setClientRole(original.isClientRole());
        target.setCompositeRoles(original.getCompositeRoles());
        target.setDescription(original.getDescription());
        target.setId(original.getId());
        target.setName(original.getName());
        target.setRealmId(original.getRealmId());
        target.clearUpdatedFlag();
        return target;
    }
    public static org.keycloak.models.map.role.MapRoleEntity deepCloneNoId(org.keycloak.models.map.role.MapRoleEntity original, org.keycloak.models.map.role.MapRoleEntity target) {
        target.setAttributes(original.getAttributes());
        target.setClientId(original.getClientId());
        target.setClientRole(original.isClientRole());
        target.setCompositeRoles(original.getCompositeRoles());
        target.setDescription(original.getDescription());
        target.setName(original.getName());
        target.setRealmId(original.getRealmId());
        target.clearUpdatedFlag();
        return target;
    }
}
