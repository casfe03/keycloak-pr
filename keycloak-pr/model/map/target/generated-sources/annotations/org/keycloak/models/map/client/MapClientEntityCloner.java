package org.keycloak.models.map.client;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapClientEntityCloner {
    public static org.keycloak.models.map.client.MapClientEntity deepClone(org.keycloak.models.map.client.MapClientEntity original, org.keycloak.models.map.client.MapClientEntity target) {
        target.setAlwaysDisplayInConsole(original.isAlwaysDisplayInConsole());
        if (original.getAttributes() != null) {
            original.getAttributes().forEach(target::setAttribute);
        }
        target.setAuthFlowBindings(original.getAuthFlowBindings());
        if (original.getAuthenticationFlowBindingOverrides() != null) {
            original.getAuthenticationFlowBindingOverrides().forEach(target::setAuthenticationFlowBindingOverride);
        }
        target.setBaseUrl(original.getBaseUrl());
        target.setBearerOnly(original.isBearerOnly());
        target.setClientAuthenticatorType(original.getClientAuthenticatorType());
        target.setClientId(original.getClientId());
        if (original.getClientScopes() != null) {
            original.getClientScopes().forEach(target::setClientScope);
        }
        target.setConsentRequired(original.isConsentRequired());
        target.setDescription(original.getDescription());
        target.setDirectAccessGrantsEnabled(original.isDirectAccessGrantsEnabled());
        target.setEnabled(original.isEnabled());
        target.setFrontchannelLogout(original.isFrontchannelLogout());
        target.setFullScopeAllowed(original.isFullScopeAllowed());
        target.setId(original.getId());
        target.setImplicitFlowEnabled(original.isImplicitFlowEnabled());
        target.setManagementUrl(original.getManagementUrl());
        target.setName(original.getName());
        target.setNodeReRegistrationTimeout(original.getNodeReRegistrationTimeout());
        target.setNotBefore(original.getNotBefore());
        target.setProtocol(original.getProtocol());
        if (original.getProtocolMappers() != null) {
            original.getProtocolMappers().forEach(target::setProtocolMapper);
        }
        target.setPublicClient(original.isPublicClient());
        target.setRealmId(original.getRealmId());
        target.setRedirectUris(original.getRedirectUris());
        target.setRegistrationToken(original.getRegistrationToken());
        target.setRootUrl(original.getRootUrl());
        target.setScope(original.getScope());
        if (original.getScopeMappings() != null) {
            original.getScopeMappings().forEach(target::addScopeMapping);
        }
        target.setSecret(original.getSecret());
        target.setServiceAccountsEnabled(original.isServiceAccountsEnabled());
        target.setStandardFlowEnabled(original.isStandardFlowEnabled());
        target.setSurrogateAuthRequired(original.isSurrogateAuthRequired());
        target.setWebOrigins(original.getWebOrigins());
        target.clearUpdatedFlag();
        return target;
    }
    public static org.keycloak.models.map.client.MapClientEntity deepCloneNoId(org.keycloak.models.map.client.MapClientEntity original, org.keycloak.models.map.client.MapClientEntity target) {
        target.setAlwaysDisplayInConsole(original.isAlwaysDisplayInConsole());
        if (original.getAttributes() != null) {
            original.getAttributes().forEach(target::setAttribute);
        }
        target.setAuthFlowBindings(original.getAuthFlowBindings());
        if (original.getAuthenticationFlowBindingOverrides() != null) {
            original.getAuthenticationFlowBindingOverrides().forEach(target::setAuthenticationFlowBindingOverride);
        }
        target.setBaseUrl(original.getBaseUrl());
        target.setBearerOnly(original.isBearerOnly());
        target.setClientAuthenticatorType(original.getClientAuthenticatorType());
        target.setClientId(original.getClientId());
        if (original.getClientScopes() != null) {
            original.getClientScopes().forEach(target::setClientScope);
        }
        target.setConsentRequired(original.isConsentRequired());
        target.setDescription(original.getDescription());
        target.setDirectAccessGrantsEnabled(original.isDirectAccessGrantsEnabled());
        target.setEnabled(original.isEnabled());
        target.setFrontchannelLogout(original.isFrontchannelLogout());
        target.setFullScopeAllowed(original.isFullScopeAllowed());
        target.setImplicitFlowEnabled(original.isImplicitFlowEnabled());
        target.setManagementUrl(original.getManagementUrl());
        target.setName(original.getName());
        target.setNodeReRegistrationTimeout(original.getNodeReRegistrationTimeout());
        target.setNotBefore(original.getNotBefore());
        target.setProtocol(original.getProtocol());
        if (original.getProtocolMappers() != null) {
            original.getProtocolMappers().forEach(target::setProtocolMapper);
        }
        target.setPublicClient(original.isPublicClient());
        target.setRealmId(original.getRealmId());
        target.setRedirectUris(original.getRedirectUris());
        target.setRegistrationToken(original.getRegistrationToken());
        target.setRootUrl(original.getRootUrl());
        target.setScope(original.getScope());
        if (original.getScopeMappings() != null) {
            original.getScopeMappings().forEach(target::addScopeMapping);
        }
        target.setSecret(original.getSecret());
        target.setServiceAccountsEnabled(original.isServiceAccountsEnabled());
        target.setStandardFlowEnabled(original.isStandardFlowEnabled());
        target.setSurrogateAuthRequired(original.isSurrogateAuthRequired());
        target.setWebOrigins(original.getWebOrigins());
        target.clearUpdatedFlag();
        return target;
    }
}
