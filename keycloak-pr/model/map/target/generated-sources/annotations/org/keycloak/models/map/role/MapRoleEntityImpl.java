package org.keycloak.models.map.role;
import java.util.Objects;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapRoleEntityImpl extends org.keycloak.models.map.role.MapRoleEntity.AbstractRoleEntity implements org.keycloak.models.map.role.MapRoleEntity {
    public MapRoleEntityImpl() { super(); }
    @Override public boolean equals(Object o) {
        if (o == this) return true; 
        if (! (o instanceof MapRoleEntityImpl)) return false; 
        MapRoleEntityImpl other = (MapRoleEntityImpl) o; 
        return Objects.equals(getId(), other.getId())
          && Objects.equals(getAttributes(), other.getAttributes())
          && Objects.equals(getClientId(), other.getClientId())
          && Objects.equals(getCompositeRoles(), other.getCompositeRoles())
          && Objects.equals(getDescription(), other.getDescription())
          && Objects.equals(getName(), other.getName())
          && Objects.equals(getRealmId(), other.getRealmId())
          && Objects.equals(isClientRole(), other.isClientRole());
    }
    @Override public int hashCode() {
        return (getId() == null ? super.hashCode() : getId().hashCode());
    }
    @Override public String toString() {
        return String.format("%s@%08x", getId(), System.identityHashCode(this));
    }

    private String fId;

    private java.util.Map<String,java.util.List<String>> fAttributes;
    @SuppressWarnings("unchecked") @Override public void removeAttribute(String p0) {
        if (fAttributes == null) { return; }
        boolean removed = fAttributes.remove(p0) != null;
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public void setAttributes(java.util.Map<String,java.util.List<String>> p0) {
        p0 = p0 == null ? null : new java.util.HashMap<>(p0);
        updated |= ! Objects.equals(fAttributes, p0);
        fAttributes = p0;
    }
    @SuppressWarnings("unchecked") @Override public void setAttribute(String p0, java.util.List<String> p1) {
        if (fAttributes == null) { fAttributes = new java.util.HashMap<>(); }
        p1 = p1 == null ? null : new java.util.LinkedList<>(p1);
        Object v = fAttributes.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return fAttributes;
    }

    private String fClientId;
    @SuppressWarnings("unchecked") @Override public String getClientId() {
        return fClientId;
    }
    @SuppressWarnings("unchecked") @Override public void setClientId(String p0) {
        updated |= ! Objects.equals(fClientId, p0);
        fClientId = p0;
    }

    private Boolean fClientRole;
    @SuppressWarnings("unchecked") @Override public void setClientRole(Boolean p0) {
        updated |= ! Objects.equals(fClientRole, p0);
        fClientRole = p0;
    }
    @SuppressWarnings("unchecked") @Override public Boolean isClientRole() {
        return fClientRole;
    }

    private java.util.Set<String> fCompositeRoles;
    @SuppressWarnings("unchecked") @Override public void removeCompositeRole(String p0) {
        if (fCompositeRoles == null) { return; }
        boolean removed = fCompositeRoles.remove(p0);
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Set<String> getCompositeRoles() {
        return fCompositeRoles;
    }
    @SuppressWarnings("unchecked") @Override public void setCompositeRoles(java.util.Set<String> p0) {
        p0 = p0 == null ? null : new java.util.HashSet<>(p0);
        updated |= ! Objects.equals(fCompositeRoles, p0);
        fCompositeRoles = p0;
    }
    @SuppressWarnings("unchecked") @Override public void addCompositeRole(String p0) {
        if (fCompositeRoles == null) { fCompositeRoles = new java.util.HashSet<>(); }
        updated |= fCompositeRoles.add(p0);
    }

    private String fDescription;
    @SuppressWarnings("unchecked") @Override public void setDescription(String p0) {
        updated |= ! Objects.equals(fDescription, p0);
        fDescription = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getDescription() {
        return fDescription;
    }

    private String fName;
    @SuppressWarnings("unchecked") @Override public void setName(String p0) {
        updated |= ! Objects.equals(fName, p0);
        fName = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getName() {
        return fName;
    }

    private String fRealmId;
    @SuppressWarnings("unchecked") @Override public void setRealmId(String p0) {
        updated |= ! Objects.equals(fRealmId, p0);
        fRealmId = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getRealmId() {
        return fRealmId;
    }
}
