package org.keycloak.models.map.group;
import java.util.Objects;
import org.keycloak.models.map.common.DeepCloner;
// DO NOT CHANGE THIS CLASS, IT IS GENERATED AUTOMATICALLY BY GenerateEntityImplementationsProcessor
public class MapGroupEntityImpl extends org.keycloak.models.map.group.MapGroupEntity.AbstractGroupEntity implements org.keycloak.models.map.group.MapGroupEntity {
    public MapGroupEntityImpl() { super(); }
    @Override public boolean equals(Object o) {
        if (o == this) return true; 
        if (! (o instanceof MapGroupEntityImpl)) return false; 
        MapGroupEntityImpl other = (MapGroupEntityImpl) o; 
        return Objects.equals(getId(), other.getId())
          && Objects.equals(getAttributes(), other.getAttributes())
          && Objects.equals(getGrantedRoles(), other.getGrantedRoles())
          && Objects.equals(getName(), other.getName())
          && Objects.equals(getParentId(), other.getParentId())
          && Objects.equals(getRealmId(), other.getRealmId());
    }
    @Override public int hashCode() {
        return (getId() == null ? super.hashCode() : getId().hashCode());
    }
    @Override public String toString() {
        return String.format("%s@%08x", getId(), System.identityHashCode(this));
    }

    private String fId;

    private java.util.Map<String,java.util.List<String>> fAttributes;
    @SuppressWarnings("unchecked") @Override public void setAttribute(String p0, java.util.List<String> p1) {
        if (fAttributes == null) { fAttributes = new java.util.HashMap<>(); }
        p1 = p1 == null ? null : new java.util.LinkedList<>(p1);
        Object v = fAttributes.put(p0, p1);
        updated |= ! Objects.equals(v, p1);
    }
    @SuppressWarnings("unchecked") @Override public java.util.List<String> getAttribute(String p0) {
        return fAttributes == null ? null : fAttributes.get(p0);
    }
    @SuppressWarnings("unchecked") @Override public void removeAttribute(String p0) {
        if (fAttributes == null) { return; }
        boolean removed = fAttributes.remove(p0) != null;
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Map<String,java.util.List<String>> getAttributes() {
        return fAttributes;
    }
    @SuppressWarnings("unchecked") @Override public void setAttributes(java.util.Map<String,java.util.List<String>> p0) {
        p0 = p0 == null ? null : new java.util.HashMap<>(p0);
        updated |= ! Objects.equals(fAttributes, p0);
        fAttributes = p0;
    }

    private java.util.Set<String> fGrantedRoles;
    @SuppressWarnings("unchecked") @Override public void removeGrantedRole(String p0) {
        if (fGrantedRoles == null) { return; }
        boolean removed = fGrantedRoles.remove(p0);
        updated |= removed;
    }
    @SuppressWarnings("unchecked") @Override public java.util.Set<String> getGrantedRoles() {
        return fGrantedRoles;
    }
    @SuppressWarnings("unchecked") @Override public void setGrantedRoles(java.util.Set<String> p0) {
        p0 = p0 == null ? null : new java.util.HashSet<>(p0);
        updated |= ! Objects.equals(fGrantedRoles, p0);
        fGrantedRoles = p0;
    }
    @SuppressWarnings("unchecked") @Override public void addGrantedRole(String p0) {
        if (fGrantedRoles == null) { fGrantedRoles = new java.util.HashSet<>(); }
        updated |= fGrantedRoles.add(p0);
    }

    private String fName;
    @SuppressWarnings("unchecked") @Override public String getName() {
        return fName;
    }
    @SuppressWarnings("unchecked") @Override public void setName(String p0) {
        updated |= ! Objects.equals(fName, p0);
        fName = p0;
    }

    private String fParentId;
    @SuppressWarnings("unchecked") @Override public String getParentId() {
        return fParentId;
    }
    @SuppressWarnings("unchecked") @Override public void setParentId(String p0) {
        updated |= ! Objects.equals(fParentId, p0);
        fParentId = p0;
    }

    private String fRealmId;
    @SuppressWarnings("unchecked") @Override public void setRealmId(String p0) {
        updated |= ! Objects.equals(fRealmId, p0);
        fRealmId = p0;
    }
    @SuppressWarnings("unchecked") @Override public String getRealmId() {
        return fRealmId;
    }
}
