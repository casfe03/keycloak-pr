/*
 Generated by org.infinispan.protostream.annotations.impl.processor.MarshallerSourceCodeGenerator
 for class org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity
*/

package org.keycloak.models.map.storage.hotRod.client;

/**
 * WARNING: Generated code! Do not edit!
 *
 * @private
 */
@javax.annotation.Generated(
   value = "org.infinispan.protostream.annotations.impl.processor.AutoProtoSchemaBuilderAnnotationProcessor",
   comments = "Please do not edit this file!"
)
@org.infinispan.protostream.annotations.impl.OriginatingClasses({
   "org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity"
})
@SuppressWarnings("unchecked")
public final class HotRodAttributeEntity$___Marshaller_2112b966ae83e75020f9e9469c6f25991c971ef949743800573197e545c7842c extends org.infinispan.protostream.annotations.impl.GeneratedMarshallerBase implements org.infinispan.protostream.RawProtobufMarshaller<org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity> {

   @Override
   public Class<org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity> getJavaClass() { return org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity.class; }
   
   @Override
   public String getTypeName() { return "org.keycloak.models.map.storage.hotrod.HotRodAttributeEntity"; }
   
   @Override
   public org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity readFrom(org.infinispan.protostream.ImmutableSerializationContext $1, org.infinispan.protostream.RawProtoStreamReader $2) throws java.io.IOException {
      final org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity o = new org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity();
      java.util.ArrayList __c$2 = new java.util.ArrayList();
      boolean done = false;
      while (!done) {
         final int tag = $2.readTag();
         switch (tag) {
            case 0:
               done = true;
               break;
            case 10: {
               java.lang.String __v$1 = $2.readString();
               o.name = __v$1;
               break;
            }
            case 18: {
               java.lang.String __v$2 = $2.readString();
               __c$2.add(__v$2);
               break;
            }
            default: {
               if (!$2.skipField(tag)) done = true;
            }
         }
      }
      o.values = __c$2;
      
      return o;
   }
   
   @Override
   public void writeTo(org.infinispan.protostream.ImmutableSerializationContext $1, org.infinispan.protostream.RawProtoStreamWriter $2, org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity $3) throws java.io.IOException {
      final org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity o = (org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity) $3;
      {
         final java.lang.String __v$1 = o.name;
         if (__v$1 != null) $2.writeString(1, __v$1);
      }
      {
         final java.util.Collection __c$2 = o.values;
         if (__c$2 != null) 
            for (java.util.Iterator it = __c$2.iterator(); it.hasNext(); ) {
               final java.lang.String __v$2 = (java.lang.String) it.next();
               $2.writeString(2, __v$2);
            }
      }
   }
}
