/*
 Generated by org.infinispan.protostream.annotations.impl.processor.MarshallerSourceCodeGenerator
 for class org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity
*/

package org.keycloak.models.map.storage.hotRod.client;

/**
 * WARNING: Generated code! Do not edit!
 *
 * @private
 */
@javax.annotation.Generated(
   value = "org.infinispan.protostream.annotations.impl.processor.AutoProtoSchemaBuilderAnnotationProcessor",
   comments = "Please do not edit this file!"
)
@org.infinispan.protostream.annotations.impl.OriginatingClasses({
   "org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity"
})
@SuppressWarnings("unchecked")
public final class HotRodClientEntity$___Marshaller_a48aa5c2c8d04966a0c683ee0d81359c14d0e2894f070453f9c8bffb982af3d7 extends org.infinispan.protostream.annotations.impl.GeneratedMarshallerBase implements org.infinispan.protostream.RawProtobufMarshaller<org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity> {

   private org.infinispan.protostream.impl.BaseMarshallerDelegate __md$14;
   
   private org.infinispan.protostream.impl.BaseMarshallerDelegate __md$15;
   
   private org.infinispan.protostream.impl.BaseMarshallerDelegate __md$22;
   
   private org.infinispan.protostream.impl.BaseMarshallerDelegate __md$23;
   
   @Override
   public Class<org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity> getJavaClass() { return org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity.class; }
   
   @Override
   public String getTypeName() { return "org.keycloak.models.map.storage.hotrod.HotRodClientEntity"; }
   
   @Override
   public org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity readFrom(org.infinispan.protostream.ImmutableSerializationContext $1, org.infinispan.protostream.RawProtoStreamReader $2) throws java.io.IOException {
      final org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity o = new org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity();
      long __bits$0 = 0;
      java.util.HashSet __c$7 = new java.util.HashSet();
      java.util.HashSet __c$14 = new java.util.HashSet();
      java.util.HashSet __c$15 = new java.util.HashSet();
      java.util.HashSet __c$20 = new java.util.HashSet();
      java.util.HashSet __c$21 = new java.util.HashSet();
      java.util.HashSet __c$22 = new java.util.HashSet();
      java.util.HashSet __c$23 = new java.util.HashSet();
      java.util.HashSet __c$24 = new java.util.HashSet();
      boolean done = false;
      while (!done) {
         final int tag = $2.readTag();
         switch (tag) {
            case 0:
               done = true;
               break;
            case 8: {
               int __v$1 = $2.readInt32();
               o.entityVersion = __v$1;
               __bits$0 |= 1L;
               break;
            }
            case 18: {
               java.lang.String __v$2 = $2.readString();
               o.id = __v$2;
               __bits$0 |= 2L;
               break;
            }
            case 26: {
               java.lang.String __v$3 = $2.readString();
               o.realmId = __v$3;
               break;
            }
            case 34: {
               java.lang.String __v$4 = $2.readString();
               o.clientId = __v$4;
               break;
            }
            case 42: {
               java.lang.String __v$5 = $2.readString();
               o.name = __v$5;
               break;
            }
            case 50: {
               java.lang.String __v$6 = $2.readString();
               o.description = __v$6;
               break;
            }
            case 58: {
               java.lang.String __v$7 = $2.readString();
               __c$7.add(__v$7);
               break;
            }
            case 64: {
               java.lang.Boolean __v$8 = new java.lang.Boolean($2.readBool());
               o.enabled = __v$8;
               break;
            }
            case 72: {
               java.lang.Boolean __v$9 = new java.lang.Boolean($2.readBool());
               o.alwaysDisplayInConsole = __v$9;
               break;
            }
            case 82: {
               java.lang.String __v$10 = $2.readString();
               o.clientAuthenticatorType = __v$10;
               break;
            }
            case 90: {
               java.lang.String __v$11 = $2.readString();
               o.secret = __v$11;
               break;
            }
            case 98: {
               java.lang.String __v$12 = $2.readString();
               o.registrationToken = __v$12;
               break;
            }
            case 106: {
               java.lang.String __v$13 = $2.readString();
               o.protocol = __v$13;
               break;
            }
            case 114: {
               if (__md$14 == null) __md$14 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity.class);
               int length = $2.readRawVarint32();
               int oldLimit = $2.pushLimit(length);
               org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity __v$14 = (org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity) readMessage(__md$14, $2);
               $2.checkLastTagWas(0);
               $2.popLimit(oldLimit);
               __c$14.add(__v$14);
               break;
            }
            case 122: {
               if (__md$15 == null) __md$15 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.common.HotRodPair.class);
               int length = $2.readRawVarint32();
               int oldLimit = $2.pushLimit(length);
               org.keycloak.models.map.storage.hotRod.common.HotRodPair __v$15 = (org.keycloak.models.map.storage.hotRod.common.HotRodPair) readMessage(__md$15, $2);
               $2.checkLastTagWas(0);
               $2.popLimit(oldLimit);
               __c$15.add(__v$15);
               break;
            }
            case 128: {
               java.lang.Boolean __v$16 = new java.lang.Boolean($2.readBool());
               o.publicClient = __v$16;
               break;
            }
            case 136: {
               java.lang.Boolean __v$17 = new java.lang.Boolean($2.readBool());
               o.fullScopeAllowed = __v$17;
               break;
            }
            case 144: {
               java.lang.Boolean __v$18 = new java.lang.Boolean($2.readBool());
               o.frontchannelLogout = __v$18;
               break;
            }
            case 152: {
               java.lang.Integer __v$19 = new java.lang.Integer($2.readInt32());
               o.notBefore = __v$19;
               break;
            }
            case 162: {
               java.lang.String __v$20 = $2.readString();
               __c$20.add(__v$20);
               break;
            }
            case 170: {
               java.lang.String __v$21 = $2.readString();
               __c$21.add(__v$21);
               break;
            }
            case 178: {
               if (__md$22 == null) __md$22 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity.class);
               int length = $2.readRawVarint32();
               int oldLimit = $2.pushLimit(length);
               org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity __v$22 = (org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity) readMessage(__md$22, $2);
               $2.checkLastTagWas(0);
               $2.popLimit(oldLimit);
               __c$22.add(__v$22);
               break;
            }
            case 186: {
               if (__md$23 == null) __md$23 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.common.HotRodPair.class);
               int length = $2.readRawVarint32();
               int oldLimit = $2.pushLimit(length);
               org.keycloak.models.map.storage.hotRod.common.HotRodPair __v$23 = (org.keycloak.models.map.storage.hotRod.common.HotRodPair) readMessage(__md$23, $2);
               $2.checkLastTagWas(0);
               $2.popLimit(oldLimit);
               __c$23.add(__v$23);
               break;
            }
            case 194: {
               java.lang.String __v$24 = $2.readString();
               __c$24.add(__v$24);
               break;
            }
            case 200: {
               java.lang.Boolean __v$25 = new java.lang.Boolean($2.readBool());
               o.surrogateAuthRequired = __v$25;
               break;
            }
            case 210: {
               java.lang.String __v$26 = $2.readString();
               o.managementUrl = __v$26;
               break;
            }
            case 218: {
               java.lang.String __v$27 = $2.readString();
               o.baseUrl = __v$27;
               break;
            }
            case 224: {
               java.lang.Boolean __v$28 = new java.lang.Boolean($2.readBool());
               o.bearerOnly = __v$28;
               break;
            }
            case 232: {
               java.lang.Boolean __v$29 = new java.lang.Boolean($2.readBool());
               o.consentRequired = __v$29;
               break;
            }
            case 242: {
               java.lang.String __v$30 = $2.readString();
               o.rootUrl = __v$30;
               break;
            }
            case 248: {
               java.lang.Boolean __v$31 = new java.lang.Boolean($2.readBool());
               o.standardFlowEnabled = __v$31;
               break;
            }
            case 256: {
               java.lang.Boolean __v$32 = new java.lang.Boolean($2.readBool());
               o.implicitFlowEnabled = __v$32;
               break;
            }
            case 264: {
               java.lang.Boolean __v$33 = new java.lang.Boolean($2.readBool());
               o.directAccessGrantsEnabled = __v$33;
               break;
            }
            case 272: {
               java.lang.Boolean __v$34 = new java.lang.Boolean($2.readBool());
               o.serviceAccountsEnabled = __v$34;
               break;
            }
            case 280: {
               java.lang.Integer __v$35 = new java.lang.Integer($2.readInt32());
               o.nodeReRegistrationTimeout = __v$35;
               break;
            }
            default: {
               if (!$2.skipField(tag)) done = true;
            }
         }
      }
      o.redirectUris = __c$7;
      
      o.attributes = __c$14;
      
      o.authFlowBindings = __c$15;
      
      o.scope = __c$20;
      
      o.webOrigins = __c$21;
      
      o.protocolMappers = __c$22;
      
      o.clientScopes = __c$23;
      
      o.scopeMappings = __c$24;
      
      if ((__bits$0 & 3L) != 3L) {
         final StringBuilder missing = new StringBuilder();
         if ((__bits$0 & 1L) == 0) {
            missing.append("entityVersion");
         }
         if ((__bits$0 & 2L) == 0) {
            if (missing.length() > 0) missing.append(", ");
            missing.append("id");
         }
         throw new java.io.IOException("Required field(s) missing from input stream : " + missing);
      }
      return o;
   }
   
   @Override
   public void writeTo(org.infinispan.protostream.ImmutableSerializationContext $1, org.infinispan.protostream.RawProtoStreamWriter $2, org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity $3) throws java.io.IOException {
      final org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity o = (org.keycloak.models.map.storage.hotRod.client.HotRodClientEntity) $3;
      {
         final int __v$1 = o.entityVersion;
         $2.writeInt32(1, __v$1);
      }
      {
         final java.lang.String __v$2 = o.id;
         if (__v$2 == null) throw new IllegalStateException("Required field must not be null : id");
         $2.writeString(2, __v$2);
      }
      {
         final java.lang.String __v$3 = o.realmId;
         if (__v$3 != null) $2.writeString(3, __v$3);
      }
      {
         final java.lang.String __v$4 = o.clientId;
         if (__v$4 != null) $2.writeString(4, __v$4);
      }
      {
         final java.lang.String __v$5 = o.name;
         if (__v$5 != null) $2.writeString(5, __v$5);
      }
      {
         final java.lang.String __v$6 = o.description;
         if (__v$6 != null) $2.writeString(6, __v$6);
      }
      {
         final java.util.Collection __c$7 = o.redirectUris;
         if (__c$7 != null) 
            for (java.util.Iterator it = __c$7.iterator(); it.hasNext(); ) {
               final java.lang.String __v$7 = (java.lang.String) it.next();
               $2.writeString(7, __v$7);
            }
      }
      {
         final java.lang.Boolean __v$8 = o.enabled;
         if (__v$8 != null) $2.writeBool(8, __v$8.booleanValue());
      }
      {
         final java.lang.Boolean __v$9 = o.alwaysDisplayInConsole;
         if (__v$9 != null) $2.writeBool(9, __v$9.booleanValue());
      }
      {
         final java.lang.String __v$10 = o.clientAuthenticatorType;
         if (__v$10 != null) $2.writeString(10, __v$10);
      }
      {
         final java.lang.String __v$11 = o.secret;
         if (__v$11 != null) $2.writeString(11, __v$11);
      }
      {
         final java.lang.String __v$12 = o.registrationToken;
         if (__v$12 != null) $2.writeString(12, __v$12);
      }
      {
         final java.lang.String __v$13 = o.protocol;
         if (__v$13 != null) $2.writeString(13, __v$13);
      }
      {
         final java.util.Collection __c$14 = o.attributes;
         if (__c$14 != null) 
            for (java.util.Iterator it = __c$14.iterator(); it.hasNext(); ) {
               final org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity __v$14 = (org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity) it.next();
               {
                  if (__md$14 == null) __md$14 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.client.HotRodAttributeEntity.class);
                  writeNestedMessage(__md$14, $2, 14, __v$14);
               }
            }
      }
      {
         final java.util.Collection __c$15 = o.authFlowBindings;
         if (__c$15 != null) 
            for (java.util.Iterator it = __c$15.iterator(); it.hasNext(); ) {
               final org.keycloak.models.map.storage.hotRod.common.HotRodPair __v$15 = (org.keycloak.models.map.storage.hotRod.common.HotRodPair) it.next();
               {
                  if (__md$15 == null) __md$15 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.common.HotRodPair.class);
                  writeNestedMessage(__md$15, $2, 15, __v$15);
               }
            }
      }
      {
         final java.lang.Boolean __v$16 = o.publicClient;
         if (__v$16 != null) $2.writeBool(16, __v$16.booleanValue());
      }
      {
         final java.lang.Boolean __v$17 = o.fullScopeAllowed;
         if (__v$17 != null) $2.writeBool(17, __v$17.booleanValue());
      }
      {
         final java.lang.Boolean __v$18 = o.frontchannelLogout;
         if (__v$18 != null) $2.writeBool(18, __v$18.booleanValue());
      }
      {
         final java.lang.Integer __v$19 = o.notBefore;
         if (__v$19 != null) $2.writeInt32(19, __v$19.intValue());
      }
      {
         final java.util.Collection __c$20 = o.scope;
         if (__c$20 != null) 
            for (java.util.Iterator it = __c$20.iterator(); it.hasNext(); ) {
               final java.lang.String __v$20 = (java.lang.String) it.next();
               $2.writeString(20, __v$20);
            }
      }
      {
         final java.util.Collection __c$21 = o.webOrigins;
         if (__c$21 != null) 
            for (java.util.Iterator it = __c$21.iterator(); it.hasNext(); ) {
               final java.lang.String __v$21 = (java.lang.String) it.next();
               $2.writeString(21, __v$21);
            }
      }
      {
         final java.util.Collection __c$22 = o.protocolMappers;
         if (__c$22 != null) 
            for (java.util.Iterator it = __c$22.iterator(); it.hasNext(); ) {
               final org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity __v$22 = (org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity) it.next();
               {
                  if (__md$22 == null) __md$22 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.client.HotRodProtocolMapperEntity.class);
                  writeNestedMessage(__md$22, $2, 22, __v$22);
               }
            }
      }
      {
         final java.util.Collection __c$23 = o.clientScopes;
         if (__c$23 != null) 
            for (java.util.Iterator it = __c$23.iterator(); it.hasNext(); ) {
               final org.keycloak.models.map.storage.hotRod.common.HotRodPair __v$23 = (org.keycloak.models.map.storage.hotRod.common.HotRodPair) it.next();
               {
                  if (__md$23 == null) __md$23 = ((org.infinispan.protostream.impl.SerializationContextImpl) $1).getMarshallerDelegate(org.keycloak.models.map.storage.hotRod.common.HotRodPair.class);
                  writeNestedMessage(__md$23, $2, 23, __v$23);
               }
            }
      }
      {
         final java.util.Collection __c$24 = o.scopeMappings;
         if (__c$24 != null) 
            for (java.util.Iterator it = __c$24.iterator(); it.hasNext(); ) {
               final java.lang.String __v$24 = (java.lang.String) it.next();
               $2.writeString(24, __v$24);
            }
      }
      {
         final java.lang.Boolean __v$25 = o.surrogateAuthRequired;
         if (__v$25 != null) $2.writeBool(25, __v$25.booleanValue());
      }
      {
         final java.lang.String __v$26 = o.managementUrl;
         if (__v$26 != null) $2.writeString(26, __v$26);
      }
      {
         final java.lang.String __v$27 = o.baseUrl;
         if (__v$27 != null) $2.writeString(27, __v$27);
      }
      {
         final java.lang.Boolean __v$28 = o.bearerOnly;
         if (__v$28 != null) $2.writeBool(28, __v$28.booleanValue());
      }
      {
         final java.lang.Boolean __v$29 = o.consentRequired;
         if (__v$29 != null) $2.writeBool(29, __v$29.booleanValue());
      }
      {
         final java.lang.String __v$30 = o.rootUrl;
         if (__v$30 != null) $2.writeString(30, __v$30);
      }
      {
         final java.lang.Boolean __v$31 = o.standardFlowEnabled;
         if (__v$31 != null) $2.writeBool(31, __v$31.booleanValue());
      }
      {
         final java.lang.Boolean __v$32 = o.implicitFlowEnabled;
         if (__v$32 != null) $2.writeBool(32, __v$32.booleanValue());
      }
      {
         final java.lang.Boolean __v$33 = o.directAccessGrantsEnabled;
         if (__v$33 != null) $2.writeBool(33, __v$33.booleanValue());
      }
      {
         final java.lang.Boolean __v$34 = o.serviceAccountsEnabled;
         if (__v$34 != null) $2.writeBool(34, __v$34.booleanValue());
      }
      {
         final java.lang.Integer __v$35 = o.nodeReRegistrationTimeout;
         if (__v$35 != null) $2.writeInt32(35, __v$35.intValue());
      }
   }
}
